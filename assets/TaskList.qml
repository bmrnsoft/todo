import bb.cascades 1.0
  
Container {
    layout: StackLayout {
        orientation: LayoutOrientation.TopToBottom
    }
    ListView {
        listItemComponents: [
            ListItemComponent {
                type: "task"
                Container {
                    id: itemRoot
                    Label {
                        text: "Hello"
                    }
                    Label {
                        text: "World"
                    }
                }
            }
        ]
    }
    
}