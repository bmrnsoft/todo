// Default empty project template
import bb.cascades 1.0

// creates one page with a label
Page {
    Container {
        attachedObjects: [
            Sheet {
                id: createTaskSheet
                CreateTaskView {
                    id: createTaskView
                    onAddTaskClose: {
                        createTaskSheet.close();
                    }
                }
            }
        ]
        
        layout: DockLayout {}
        Label {
            text: qsTr("Hello World")
            textStyle.base: SystemDefaults.TextStyles.BigText
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        TaskList {
            id: mainTaskList
        }
        Container {
	        Button {
	            text: "Touch me"
	            onClicked: {
                    createTaskSheet.open();
	            }
	        }
         }
    }
}

