import bb.cascades 1.0

Page {
    id: addTask
    signal addTaskClose()
    titleBar: TitleBar {
        id: addTaskTitle
        title: "New Task"
        visibility: ChromeVisibility.Visible
        
        dismissAction: ActionItem {
            title: "Cancel"
            onTriggered: {
                addTask.addTaskClose();
            }
        }
        
        acceptAction: ActionItem {
            title: "Save"
        }
    }
    Container {
        id: taskEditPane
        layout: StackLayout {
        }
        Container {
            id: taskEditContainer
            property real padding: 50
            background: Color.create("#f8f8f8")
            topPadding: taskEditContainer.padding
            leftPadding: taskEditContainer.padding
            rightPadding: taskEditContainer.padding
            TextArea {
	            id: taskEnterArea
	            hintText: "Enter Task"
	            topMargin: taskEditContainer.padding
	            bottomMargin: topMargin
	            preferredHeight: 450
	            maxHeight: 450
	            horizontalAlignment: HorizontalAlignment.Fill
	        }
	        SegmentedControl {
	            id: taskTypeControl
	            bottomMargin: taskEditContainer.padding
	            Option {
	            	id: personalOption
	            	text: "Personal"
	            	value: "personal"
	            	selected: true
	            }
	            Option {
	                id: workOption
	                text: "Work"
	                value: "work"
	            }
	
	        }
	        DateTimePicker {
	        	id: taskTimePicker
	        	title: "Due Date"
	        	value: "1980-04-15"
	            bottomMargin: taskEditContainer.padding
	        }
	    }
        ImageButton {
            id: createTaskComplete
            topMargin: taskEditContainer.padding
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center
            defaultImageSource: "asset:///images/checkBox.png"
            
            onClicked: {
                console.log("clicked");
               // taskTypeControl.selectedOption.value
                _createTask.text = taskEnterArea.text;
                _createTask.type = taskTypeControl.selectedOption.value;
                _createTask.date = taskTimePicker.value;
                _createTask.addNewTask();
            }
        }
    }
}
