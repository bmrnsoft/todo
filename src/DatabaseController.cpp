/*
 * DatabaseController.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: Brennan
 */

#include "DatabaseController.h"

#include <unistd.h>
#include <stdlib.h>

#include "QDir.h"
#include "QFile.h"
#include "QDateTime.h"

const QString TASK_TABLE_NAME = "Tasks";

DatabaseController::DatabaseController()
{
	QDir home = QDir::home();
	qDebug() << home.path();

	//Create the db if it doesn't exist.
	if(!QFile::exists(home.absoluteFilePath("sqllite.db")))
	{
		QFile file(home.absoluteFilePath("sqllite.db"));

		// Open the file that was created
		if (file.open(QIODevice::ReadWrite)) {
			// Create an SqlDataAccess object
			m_DataAccess = new SqlDataAccess(home.absoluteFilePath("sqllite.db"));

			m_DataAccess->execute("CREATE TABLE " + TASK_TABLE_NAME + " ( TASK_ID INTEGER PRIMARY KEY AUTOINCREMENT, MINUTES_SINCE_EPOCH INT, NOTE_TEXT TEXT, PARENT_ID INT );");
		}
	}
	else
	{
		//Just set the DB if it exists.
		m_DataAccess = new SqlDataAccess(home.absoluteFilePath("sqllite.db"));
	}
}

void DatabaseController::InsertTask(int minutesSinceEpoch, QString noteText, int parentId)
{
	QString insertTaskQuery;
	QTextStream(&insertTaskQuery) << "INSERT INTO " << TASK_TABLE_NAME << " (MINUTES_SINCE_EPOCH, NOTE_TEXT, PARENT_ID) ";
	QTextStream(&insertTaskQuery) << "VALUES ('" << minutesSinceEpoch << "','" << noteText <<"'," << parentId << ")";
	m_DataAccess->execute(insertTaskQuery);

	//return GetFavoriteSubreddits();
}

QList<Task> DatabaseController::GetAllTasks()
{
	QList<Task> tasks;

	QString getTasksQuery;
	QTextStream(&getTasksQuery) << "SELECT TASK_ID, MINUTES_SINCE_EPOCH, NOTE_TEXT, PARENT_ID FROM "+TASK_TABLE_NAME+";";
	QVariant result = m_DataAccess->execute(getTasksQuery);

	QVariantList resultList = result.value<QVariantList>();

	Q_FOREACH(QVariant var, resultList)
	{
		int id = var.toMap()["TASK_ID"].toInt();
		int minutes = var.toMap()["MINUTES_SINCE_EPOCH"].toInt();
		QString noteText = var.toMap()["NOTE_TEXT"].toString();
		int parentId = var.toMap()["PARENT_ID"].toInt();

		tasks.append(Task(id, minutes, noteText, parentId));
		qDebug() << "id: " << id << " minutes: " << minutes << " noteText: " << noteText << " parentId: " << parentId;
	}

	return tasks;
}


DatabaseController::~DatabaseController()
{
	// TODO Auto-generated destructor stub
}


