/*
 * DatabaseController.h
 *
 *  Created on: Jun 1, 2013
 *      Author: Brennan & Martina
 */

#ifndef DATABASECONTROLLER_H_
#define DATABASECONTROLLER_H_

#include <bb/data/SqlDataAccess>
#include "Task.h"

using namespace bb::data;

class DatabaseController
{
public:
	void InsertTask(int minutesSinceEpoch, QString noteText, int parentId);
	QList<Task> GetAllTasks();
	DatabaseController();
	virtual ~DatabaseController();

private:
	SqlDataAccess* m_DataAccess;

};

#endif /* DATABASECONTROLLER_H_ */
