/*
 * CreateTask.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Martina
 */

#include "CreateTask.h"

CreateTask::CreateTask() {
	// TODO Auto-generated constructor stub

}

void CreateTask::addNewTask()
{
	int i = 8;
}

QString CreateTask::getTaskText()
{
	return m_text;
}
void CreateTask::setTaskText(QString text)
{
	m_text = text;
	emit valueChanged(m_text);
}

QVariant CreateTask::getTaskType()
{
	return m_type;
}
void CreateTask::setTaskType(QVariant type)
{
	m_type = type;
	emit valueChanged(m_type);
}

QDateTime CreateTask::getTaskDate()
{
	return m_date;
}
void CreateTask::setTaskDate(QDateTime date)
{
	m_date = date;
	emit valueChanged(m_date);
}

