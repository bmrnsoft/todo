// Default empty project template
#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>

#include "CreateTask.h"

using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app)
: QObject(app)
{
    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    // create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    // set created root object as a scene
    app->setScene(root);

    QDateTime date = QDateTime::currentDateTime();
    int minutesSinceEpoch = date.toMSecsSinceEpoch()/1000/60/24;
    m_DatabaseAccess.InsertTask(minutesSinceEpoch,"Test1 text",1);
    m_DatabaseAccess.InsertTask(minutesSinceEpoch,"Test2 text",2);
    m_DatabaseAccess.InsertTask(minutesSinceEpoch,"Test3 text",3);
    m_DatabaseAccess.GetAllTasks();

    CreateTask *createTask = new CreateTask();
    qml->setContextProperty("_createTask", createTask);
}

