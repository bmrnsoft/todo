/*
 * Task.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: Brennan
 */

#include "Task.h"


Task::Task(int id, int minutesSinceEpoch, QString noteText, int parentId)
{
	m_Id = id;
	qint64 millisecondsSinceEpoch = minutesSinceEpoch*1000*60*24;
	m_CreationDate = QDateTime::fromMSecsSinceEpoch(millisecondsSinceEpoch);
	m_Text = noteText;
	m_ParentId = parentId;
}

Task::~Task()
{
	// TODO Auto-generated destructor stub
}

