/*
 * Task.h
 *
 *  Created on: Jun 1, 2013
 *      Author: Brennan
 */

#ifndef TASK_H_
#define TASK_H_

#include "QDateTime.h"

class Task
{
public:
	Task(int id, int minutesSinceEpoch, QString noteText, int parentId);
	virtual ~Task();

private:
	int m_Id;
	QDateTime m_CreationDate;
	QString m_Text;
	int m_ParentId;
};

#endif /* TASK_H_ */
