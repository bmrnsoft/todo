/*
 * CreateTask.h
 *
 *  Created on: Jun 9, 2013
 *      Author: Martina
 */
#ifndef CREATETASK_H_
#define CREATETASK_H_

#include <QObject>
#include <QDateTime>
#include <bb/cascades/SegmentedControl>

using namespace bb::cascades;
namespace bb { namespace cascades { class Application; }}

class CreateTask : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString text READ getTaskText WRITE setTaskText NOTIFY valueChanged)
	Q_PROPERTY(QVariant type READ getTaskType WRITE setTaskType NOTIFY valueChanged)
	Q_PROPERTY(QDateTime date READ getTaskDate WRITE setTaskDate NOTIFY valueChanged)
public:
	CreateTask();
	Q_INVOKABLE
	void addNewTask();

	QString getTaskText();
	void setTaskText(QString text);

	QVariant getTaskType();
	void setTaskType(QVariant type);

	QDateTime getTaskDate();
	void setTaskDate(QDateTime date);

	virtual ~CreateTask(){}
signals:
	void valueChanged(QString);
	void valueChanged(QVariant);
	void valueChanged(QDateTime);

private:
	QString m_text;
	QVariant m_type;
	QDateTime m_date;
};

#endif /* CREATETASK_H_ */
